# Coding Practice on [LeetCode](https://www.leetcode.com)

## Explore

- [Binary Search](Explore/BinarySearch/README.md)
- [Binary Tree](Explore/BinaryTree/README.md)
- [Binary Search Tree](Explore/BinarySearchTree/README.md)
- [Queue & Stack](Explore/QueueAndStack/README.md)
- [Linked List](Explore/LinkedList/README.md)

## Problems

- [133. Clone Graph](133CloneGraph.md)
- [138. Copy List with Random Pointer](138CopyListwithRandomPointer.md)
- [347. Top K Frequent Elements](347TopKFrequentElements.md)
- [378. Kth Smallest Element in a Sorted Matrix](378KthSmallestElementinaSortedMatrix.md)

